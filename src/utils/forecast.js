const request = require("request");

const forecast = (lon, lat, cb) => {
  const url = `https://api.openweathermap.org/data/2.5/weather?lat=${lon}&lon=${lat}&appid=77548c66c31c6d5ebbe0f0a001fdc409&units=metric`;

  request({ url, json: true }, (error, { body, statusCode }) => {
    if (error) {
      cb("Unable To Connect to Weather Service", undefined);
    } else if (statusCode === 404) {
      cb("Error Try Again", undefined);
    } else if (body.name === "") {
      cb(`Location Not Found , Try Another Location`, undefined);
    } else {
      cb(undefined, body);
    }
  });
};

module.exports = forecast;
